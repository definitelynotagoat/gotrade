package paper

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/definitelynotagoat/gotrade/strats"
	"go.uber.org/zap"
)

func Test_buy(t *testing.T) {
	logger := zap.NewNop()
	cases := []struct {
		name string
		sig  strats.Signal
		want *Trader
	}{
		{
			name: "happy path",
			sig: strats.Signal{
				Symbol: "BTC-USD",
				Price:  8567,
				Type:   strats.BUY,
			},
			want: &Trader{
				capital:    10000,
				buyCapital: 0,
				position: position{
					Symbol: "BTC-USD",
					Shares: 1.1672697560406209,
					Value:  10000,
				},
				buySwitch: false,
				logger:    logger,
				stratID:   "random_id",
			},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			trader := NewTrader(10000, "random_id", logger)
			trader.buy(tc.sig)
			assert.Equal(t, tc.want, trader)
		})
	}
}

func Test_sell(t *testing.T) {
	logger := zap.NewNop()

	type args struct {
		trader *Trader
		sig    strats.Signal
	}
	cases := []struct {
		name string
		args args
		want *Trader
	}{
		{
			name: "happy path",
			args: args{
				sig: strats.Signal{
					Symbol: "BTC-USD",
					Price:  9000,
					Type:   strats.SELL,
				},
				trader: &Trader{
					capital:    10000,
					buyCapital: 0,
					position: position{
						Symbol: "BTC-USD",
						Shares: 1.1672697560406209,
						Value:  10000,
					},
					buySwitch: false,
					logger:    logger,
					stratID:   "random_id",
				},
			},
			want: &Trader{
				capital:    10505.427804365589,
				buyCapital: 10505.427804365589,
				position: position{
					Symbol: "BTC-USD",
					Shares: 0,
					Value:  0,
				},
				buySwitch: true,
				logger:    logger,
				stratID:   "random_id",
			},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			tc.args.trader.sell(tc.args.sig)
			assert.Equal(t, tc.want, tc.args.trader)
		})
	}
}

func Test_evalSignal(t *testing.T) {
	logger := zap.NewNop()

	type args struct {
		signals []strats.Signal
	}

	cases := []struct {
		name string
		args args
		want *Trader
	}{
		{
			name: "even buy and sell sigals",
			args: args{
				signals: []strats.Signal{
					strats.Signal{
						Symbol: "BTC-USD",
						Price:  8000,
						Type:   strats.BUY,
					},
					strats.Signal{
						Symbol: "BTC-USD",
						Price:  9000,
						Type:   strats.SELL,
					},
					strats.Signal{
						Symbol: "BTC-USD",
						Price:  8500,
						Type:   strats.BUY,
					},
					strats.Signal{
						Symbol: "BTC-USD",
						Price:  8700,
						Type:   strats.SELL,
					},
				},
			},
			want: &Trader{
				capital:    11514.70588235294,
				buyCapital: 11514.70588235294,
				position: position{
					Symbol: "BTC-USD",
					Shares: 0,
					Value:  0,
				},
				buySwitch: true,
				logger:    logger,
				stratID:   "random_id",
			},
		},
		{
			name: "more buy than sell sigals",
			args: args{
				signals: []strats.Signal{
					strats.Signal{
						Symbol: "BTC-USD",
						Price:  8000,
						Type:   strats.BUY,
					},
					strats.Signal{
						Symbol: "BTC-USD",
						Price:  8500,
						Type:   strats.BUY,
					},
					strats.Signal{
						Symbol: "BTC-USD",
						Price:  9000,
						Type:   strats.SELL,
					},
				},
			},
			want: &Trader{
				capital:    11250,
				buyCapital: 11250,
				position: position{
					Symbol: "BTC-USD",
					Shares: 0,
					Value:  0,
				},
				buySwitch: true,
				logger:    logger,
				stratID:   "random_id",
			},
		},
		{
			name: "more sells than buys sigals",
			args: args{
				signals: []strats.Signal{
					strats.Signal{
						Symbol: "BTC-USD",
						Price:  8000,
						Type:   strats.BUY,
					},
					strats.Signal{
						Symbol: "BTC-USD",
						Price:  9000,
						Type:   strats.SELL,
					},
					strats.Signal{
						Symbol: "BTC-USD",
						Price:  8700,
						Type:   strats.SELL,
					},
				},
			},
			want: &Trader{
				capital:    11250,
				buyCapital: 11250,
				position: position{
					Symbol: "BTC-USD",
					Shares: 0,
					Value:  0,
				},
				buySwitch: true,
				logger:    logger,
				stratID:   "random_id",
			},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			trader := NewTrader(10000, "random_id", logger)
			ctx := context.Background()

			sigs := make(chan strats.Signal, 10)
			capital := make(chan float64, 10)
			for _, sig := range tc.args.signals {
				sigs <- sig
			}
			trader.evalSignal(ctx, sigs, capital, Metrics{}, "", 1*time.Minute)
			time.Sleep(5 * time.Millisecond)

			assert.Equal(t, tc.want, trader)

		})
	}
}

// func Test_Start(t *testing.T) {
// 	logger := zap.NewNop()
// 	type args struct {
// 		capital  float64
// 		buysigs  []strats.Signal
// 		sellsigs []strats.Signal
// 	}
// 	cases := []struct {
// 		name string
// 		args args
// 		want *Trader
// 	}{
// 		// {
// 		// 	name: "Even Buy Sell",
// 		// 	args: args{
// 		// 		capital: 10000,
// 		// 		buysigs: []strats.Signal{
// 		// 			strats.Signal{
// 		// 				Price:  7000,
// 		// 				Symbol: "BTC-USD",
// 		// 			},
// 		// 			strats.Signal{
// 		// 				Price:  6898,
// 		// 				Symbol: "BTC-USD",
// 		// 			},
// 		// 			strats.Signal{
// 		// 				Price:  6555,
// 		// 				Symbol: "BTC-USD",
// 		// 			},
// 		// 		},
// 		// 		sellsigs: []strats.Signal{
// 		// 			strats.Signal{
// 		// 				Price:  9000,
// 		// 				Symbol: "BTC-USD",
// 		// 			},
// 		// 			strats.Signal{
// 		// 				Price:  8500,
// 		// 				Symbol: "BTC-USD",
// 		// 			},
// 		// 			strats.Signal{
// 		// 				Price:  8555,
// 		// 				Symbol: "BTC-USD",
// 		// 			},
// 		// 		},
// 		// 	},
// 		// 	want: &Trader{
// 		// 		capital:    20676.99977185896,
// 		// 		buyCapital: 20676.99977185896,
// 		// 		position: position{
// 		// 			Symbol: "BTC-USD",
// 		// 		},
// 		// 		logger: logger,
// 		// 	},
// 		// },
// 		{
// 			name: "More Sells",
// 			args: args{
// 				capital: 10000,
// 				buysigs: []strats.Signal{
// 					strats.Signal{
// 						Price:  7000,
// 						Symbol: "BTC-USD",
// 					},
// 					strats.Signal{
// 						Price:  6898,
// 						Symbol: "BTC-USD",
// 					},
// 					strats.Signal{
// 						Price:  6555,
// 						Symbol: "BTC-USD",
// 					},
// 				},
// 				sellsigs: []strats.Signal{
// 					strats.Signal{
// 						Price:  9000,
// 						Symbol: "BTC-USD",
// 					},
// 					strats.Signal{
// 						Price:  8500,
// 						Symbol: "BTC-USD",
// 					},
// 					strats.Signal{
// 						Price:  8555,
// 						Symbol: "BTC-USD",
// 					},
// 					strats.Signal{
// 						Price:  20000,
// 						Symbol: "BTC-USD",
// 					},
// 					strats.Signal{
// 						Price:  2322,
// 						Symbol: "BTC-USD",
// 					},
// 				},
// 			},
// 			want: &Trader{
// 				capital:    20676.99977185896,
// 				buyCapital: 20676.99977185896,
// 				position: position{
// 					Symbol: "BTC-USD",
// 				},
// 				logger: logger,
// 			},
// 		},
// 	}

// 	for _, tc := range cases {
// 		t.Run(tc.name, func(t *testing.T) {
// 			trader := NewTrader(tc.args.capital, logger)
// 			buy := make(chan strats.Signal, 100)
// 			sell := make(chan strats.Signal, 100)
// 			ctx := context.Background()
// 			trader.Start(ctx, buy, sell, 1*time.Minute)

// 			for i, buysig := range tc.args.buysigs {
// 				fmt.Printf("Sening buy sig %v\n", buysig)
// 				buy <- buysig
// 				time.Sleep(1 * time.Millisecond)
// 				if len(tc.args.sellsigs) > i {
// 					fmt.Printf("Sending sell sig %v\n", tc.args.sellsigs[i])
// 					sell <- tc.args.sellsigs[i]
// 					tc.args.sellsigs = tc.args.sellsigs[1:]
// 					time.Sleep(1 * time.Millisecond)
// 				}
// 			}

// 			for _, sellsig := range tc.args.sellsigs {
// 				fmt.Printf("Sending sell sig %v\n", sellsig)
// 				sell <- sellsig
// 				time.Sleep(1 * time.Millisecond)
// 			}

// 			time.Sleep(10 * time.Millisecond)
// 			assert.Equal(t, tc.want, trader)
// 		})
// 	}
// }

// func Test_buyStream(t *testing.T) {
// 	logger := zap.NewNop()
// 	type args struct {
// 		capital float64
// 		sigs    []strats.Signal
// 	}

// 	cases := []struct {
// 		name string
// 		args args
// 		want *Trader
// 	}{
// 		{
// 			name: "Buy Trade Without Sell",
// 			args: args{
// 				capital: 10000,
// 				sigs: []strats.Signal{
// 					strats.Signal{
// 						Price:  7000,
// 						Symbol: "BTC-USD",
// 					},
// 					strats.Signal{
// 						Price:  6898,
// 						Symbol: "BTC-USD",
// 					},
// 					strats.Signal{
// 						Price:  6555,
// 						Symbol: "BTC-USD",
// 					},
// 				},
// 			},
// 			want: &Trader{
// 				capital:    10000,
// 				buyCapital: 0,
// 				position: position{
// 					Symbol: "BTC-USD",
// 					Shares: 1.4285714285714286,
// 					Value:  10000,
// 				},
// 				logger: logger,
// 			},
// 		},
// 	}

// 	for _, tc := range cases {
// 		t.Run(tc.name, func(t *testing.T) {

// 			trader := NewTrader(tc.args.capital, logger)
// 			buy := make(chan strats.Signal, 100)
// 			capital := make(chan float64, 100)

// 			ctx := context.Background()
// 			buysMetric := prometheus.NewCounter(
// 				prometheus.CounterOpts{
// 					Namespace: namespace,
// 					Name:      "bName",
// 					Help:      "Buy trades over time",
// 				},
// 			)

// 			capitalMetric := prometheus.NewGauge(
// 				prometheus.GaugeOpts{
// 					Namespace: namespace,
// 					Name:      "cName",
// 					Help:      "Capital over time",
// 				},
// 			)
// 			trader.buyStream(ctx, buy, capital, capitalMetric, buysMetric, 1*time.Minute, "1m")
// 			for _, sig := range tc.args.sigs {
// 				buy <- sig
// 			}
// 			time.Sleep(5 * time.Millisecond)
// 			assert.Equal(t, tc.want, trader)
// 		})
// 	}
// }

// func Test_sellStream(t *testing.T) {
// 	logger := zap.NewNop()
// 	type args struct {
// 		capital float64
// 		sigs    []strats.Signal
// 	}

// 	cases := []struct {
// 		name string
// 		args args
// 		want *Trader
// 	}{
// 		{
// 			name: "Sell Trades",
// 			args: args{
// 				capital: 10000,
// 				sigs: []strats.Signal{
// 					strats.Signal{
// 						Price:  7000,
// 						Symbol: "BTC-USD",
// 					},
// 					strats.Signal{
// 						Price:  6898,
// 						Symbol: "BTC-USD",
// 					},
// 					strats.Signal{
// 						Price:  6555,
// 						Symbol: "BTC-USD",
// 					},
// 				},
// 			},
// 			want: &Trader{
// 				capital:    10000,
// 				buyCapital: 10000,
// 				position: position{
// 					Symbol: "BTC-USD",
// 					Shares: 0,
// 					Value:  0,
// 				},
// 				logger: logger,
// 			},
// 		},
// 	}

// 	for _, tc := range cases {
// 		t.Run(tc.name, func(t *testing.T) {

// 			trader := NewTrader(tc.args.capital, logger)
// 			sell := make(chan strats.Signal, 100)
// 			capital := make(chan float64, 100)

// 			ctx := context.Background()
// 			sellMetric := prometheus.NewCounter(
// 				prometheus.CounterOpts{
// 					Namespace: namespace,
// 					Name:      "sName",
// 					Help:      "Sell trades over time",
// 				},
// 			)

// 			capitalMetric := prometheus.NewGauge(
// 				prometheus.GaugeOpts{
// 					Namespace: namespace,
// 					Name:      "cName",
// 					Help:      "Capital over time",
// 				},
// 			)
// 			trader.sellStream(ctx, sell, capital, capitalMetric, sellMetric, 1*time.Minute, "1m")
// 			for _, sig := range tc.args.sigs {
// 				sell <- sig
// 			}
// 			time.Sleep(5 * time.Millisecond)
// 			assert.Equal(t, tc.want, trader)
// 		})
// 	}
// }
