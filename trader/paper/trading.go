package paper

import (
	"context"
	"fmt"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/rs/xid"
	"go.uber.org/zap"

	"gitlab.com/definitelynotagoat/gotrade/strats"
)

const (
	namespace = "gotrade"
)

type position struct {
	Symbol string
	Shares float64
	Value  float64
}

type Trader struct {
	capital    float64
	buyCapital float64
	position   position
	buySwitch  bool
	logger     *zap.Logger
	stratID    string
}

type Metrics struct {
	counters map[string]prometheus.Counter
	capital  prometheus.Gauge
}

func NewTrader(capital float64, stratID string, logger *zap.Logger) *Trader {
	return &Trader{
		capital:    capital,
		buyCapital: capital,
		position:   position{},
		buySwitch:  true,
		logger:     logger,
		stratID:    stratID,
	}
}

// Start starts a Trader datastream
func (t *Trader) Start(ctx context.Context, sigs chan strats.Signal, interval time.Duration) (chan float64, []string) {
	capitalCh := make(chan float64, 10)
	capitalCh <- t.capital

	id := xid.New().String()

	bName := fmt.Sprintf("buy_trades_%s", id)
	sName := fmt.Sprintf("sell_trades_%s", id)
	bSigName := fmt.Sprintf("buy_signals_%s", id)
	sSigName := fmt.Sprintf("sell_signals_%s", id)
	cName := fmt.Sprintf("capital_%s", id)

	counters := make(map[string]prometheus.Counter)

	buys := prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: namespace,
			Name:      bName,
			Help:      "Buy trades over time",
		},
	)
	counters["buys"] = buys

	sells := prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: namespace,
			Name:      sName,
			Help:      "Sell trades over time",
		},
	)
	counters["sells"] = sells

	buySignal := prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: namespace,
			Name:      bSigName,
			Help:      "Buy signals over time",
		},
	)
	counters["buySignal"] = buySignal

	sellSignal := prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: namespace,
			Name:      sSigName,
			Help:      "Sell signals over time",
		},
	)
	counters["sellSignal"] = sellSignal

	capital := prometheus.NewGauge(
		prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      cName,
			Help:      "Capital over time",
		},
	)

	metrics := Metrics{
		counters: counters,
		capital:  capital,
	}

	t.logger.Info(
		"adding trader metrics",
		zap.String("sell_trades", sName),
		zap.String("buy_trades", bName),
		zap.String("buy_signals", bSigName),
		zap.String("sell_signals", sSigName),
		zap.String("capital", cName),
		zap.String("id", id),
		zap.String("strat_id", t.stratID),
		zap.String("interval", interval.String()),
	)

	prometheus.MustRegister(
		buys,
		sells,
		buySignal,
		sellSignal,
		capital,
	)

	capital.Add(t.capital)
	t.evalSignal(ctx, sigs, capitalCh, metrics, id, interval)

	return capitalCh, []string{
		bName,
		sName,
		cName,
		bSigName,
		sSigName,
	}
}

func (t *Trader) evalSignal(ctx context.Context, sigs chan strats.Signal, capitalCh chan float64, metrics Metrics, id string, interval time.Duration) {
	t.log("starting trader evaluation", true, interval, id)
	go func() {
		for sig := range sigs {
			t.logger.Debug("received signal", zap.String("signal", fmt.Sprintf("%v", sig)))
			if sig.Type == strats.BUY {
				setBuySigMetric(metrics)
				if t.buySwitch {
					setBuysMetric(metrics)
					t.buy(sig)
					if metrics.capital != nil {
						metrics.capital.Set(t.capital)
					}
					t.log("buy trade", false, interval, id)

				}
			} else if sig.Type == strats.SELL {
				setSellSigMetric(metrics)
				if !t.buySwitch {
					setSellsMetric(metrics)
					t.sell(sig)
					capitalCh <- t.capital
					setCapitalMetric(metrics, t.capital)
					t.log("sell trade", false, interval, id)
				}
			}
		}
	}()
}

func (t *Trader) buy(sig strats.Signal) {
	t.position.Symbol = sig.Symbol
	t.position.Shares = t.buyCapital / sig.Price
	t.position.Value = t.position.Shares * sig.Price
	t.buyCapital = 0
	t.buySwitch = false
}

func (t *Trader) sell(sig strats.Signal) {
	t.position.Symbol = sig.Symbol
	t.capital = t.position.Shares * sig.Price
	t.position.Shares = 0
	t.position.Value = 0
	t.buyCapital = t.capital
	t.buySwitch = true
}

func setBuySigMetric(metrics Metrics) {
	sellSig, ok := metrics.counters["buySignal"]
	if ok {
		sellSig.Add(1)
	}
}

func setBuysMetric(metrics Metrics) {
	sellSig, ok := metrics.counters["buys"]
	if ok {
		sellSig.Add(1)
	}
}

func setSellSigMetric(metrics Metrics) {
	sellSig, ok := metrics.counters["sellSignal"]
	if ok {
		sellSig.Add(1)
	}
}

func setSellsMetric(metrics Metrics) {
	sellSig, ok := metrics.counters["sells"]
	if ok {
		sellSig.Add(1)
	}
}

func setCapitalMetric(metrics Metrics, capital float64) {
	if metrics.capital != nil {
		metrics.capital.Set(capital)
	}
}

func (t *Trader) log(msg string, debug bool, interval time.Duration, id string) {
	if debug {
		t.logger.Debug(
			msg,
			zap.String("id", id),
			zap.String("strat_id", t.stratID),
			zap.String("interval", interval.String()),
			zap.Float64("capital", t.capital),
			zap.Float64("buy_capital", t.buyCapital),
			zap.String("position_symbol", t.position.Symbol),
			zap.Float64("position_shares", t.position.Shares),
			zap.Float64("position_value", t.position.Value),
		)
	} else {
		t.logger.Info(
			msg,
			zap.String("id", id),
			zap.String("strat_id", t.stratID),
			zap.String("interval", interval.String()),
			zap.Float64("capital", t.capital),
			zap.Float64("buy_capital", t.buyCapital),
			zap.String("position_symbol", t.position.Symbol),
			zap.Float64("position_shares", t.position.Shares),
			zap.Float64("position_value", t.position.Value),
		)
	}

}
