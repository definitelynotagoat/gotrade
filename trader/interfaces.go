package trader

import (
	"context"
	"time"

	"gitlab.com/definitelynotagoat/gotrade/strats"
)

// Trader is an interface for all traders
type Trader interface {
	// return a channel updating the traders capital, and a string of metric ids.
	Start(ctx context.Context, sigs chan strats.Signal, interval time.Duration) (chan float64, []string)
}
