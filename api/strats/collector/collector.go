package collector

import (
	"sort"
	"sync"
	"time"

	"go.uber.org/zap"
)

type Strategy struct {
	MetricsIDs []string    `json:"metrics_ids"`
	Strategy   string      `json:"strategy"`
	Capital    float64     `json:"capital"`
	Params     interface{} `json:"params"`
}

type Collector struct {
	Strategies     map[string]Strategy
	SortedStrats   []Strategy
	mu             *sync.Mutex
	logger         *zap.Logger
	startCapital   float64
	scrapeInterval time.Duration
}

func NewCollector(startCapital float64, scrapeInterval time.Duration, logger *zap.Logger) *Collector {
	strats := make(map[string]Strategy)
	mu := &sync.Mutex{}

	c := &Collector{
		Strategies:     strats,
		mu:             mu,
		logger:         logger,
		startCapital:   startCapital,
		SortedStrats:   []Strategy{},
		scrapeInterval: scrapeInterval,
	}

	c.computeTraders()
	return c
}

func (c *Collector) UpdateStrategy(metricID string, strat Strategy) {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.Strategies[metricID] = strat
}

func (c *Collector) GetStrategies() []Strategy {
	return c.SortedStrats
}

func (c *Collector) GetBestPerformer() Strategy {
	if len(c.Strategies) > 0 {
		return c.SortedStrats[0]
	}

	return Strategy{}
}

//Returns a the best strategy or strategies if tied
func (c *Collector) computeTraders() {
	ticker := time.NewTicker(c.scrapeInterval)
	go func() {
		for range ticker.C {
			c.sort()
		}
	}()
}

func (c *Collector) sort() {
	c.mu.Lock()
	var strategies []Strategy
	for _, v := range c.Strategies {
		strategies = append(strategies, v)
	}

	sort.SliceStable(strategies, func(i, j int) bool {
		return strategies[i].Capital > strategies[j].Capital
	})

	if len(strategies) > 0 {
		c.SortedStrats = strategies
	}
	c.mu.Unlock()
}
