package strats

import (
	"encoding/json"
	"net/http"

	"gitlab.com/definitelynotagoat/gotrade/api/strats/collector"
)

type StratAPI struct {
	api *collector.Collector
}

func NewStratAPI(api *collector.Collector) *StratAPI {
	return &StratAPI{
		api: api,
	}
}

func (s *StratAPI) StratsReport(w http.ResponseWriter, r *http.Request) {
	strats := s.api.GetStrategies()
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(strats)
}

func (s *StratAPI) WinningStratReport(w http.ResponseWriter, r *http.Request) {

	strats := s.api.GetBestPerformer()
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(strats)
}
