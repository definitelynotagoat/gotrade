FROM golang:alpine
RUN mkdir -p /go/src/gitlab.com/definitelynotagoat/gotrade/
COPY ./ /go/src/gitlab.com/definitelynotagoat/gotrade/
ENV GOPATH=/go
WORKDIR /go/src/gitlab.com/definitelynotagoat/gotrade/ 
RUN go build -o gotrade .
RUN mv ./gotrade /usr/bin/


