package mocker

import (
	"context"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/definitelynotagoat/gotrade/data"
)

var (
	streamData = []byte(`[  
		{  
		   "date":"2019-08-28",
		   "open":204.1,
		   "close":205.53,
		   "high":205.72,
		   "low":203.32,
		   "volume":15957632,
		   "uOpen":204.1,
		   "uClose":205.53,
		   "uHigh":205.72,
		   "uLow":203.32,
		   "uVolume":15957632,
		   "change":1.37,
		   "changePercent":0.671,
		   "label":"Aug 28",
		   "changeOverTime":-0.019792
		},
		{  
		   "date":"2019-08-28",
		   "open":204.1,
		   "close":205.53,
		   "high":205.72,
		   "low":203.32,
		   "volume":15957632,
		   "uOpen":204.1,
		   "uClose":205.53,
		   "uHigh":205.72,
		   "uLow":203.32,
		   "uVolume":15957632,
		   "change":1.37,
		   "changePercent":0.671,
		   "label":"Aug 28",
		   "changeOverTime":-0.019792
		},
		{  
		   "date":"2019-08-28",
		   "open":204.1,
		   "close":205.53,
		   "high":205.72,
		   "low":203.32,
		   "volume":15957632,
		   "uOpen":204.1,
		   "uClose":205.53,
		   "uHigh":205.72,
		   "uLow":203.32,
		   "uVolume":15957632,
		   "change":1.37,
		   "changePercent":0.671,
		   "label":"Aug 28",
		   "changeOverTime":-0.019792
		}
	 ]
	 `)

	goldenCandle = data.Candle{
		Date:   "2019-08-28",
		Open:   204.1,
		Close:  205.53,
		High:   205.72,
		Low:    203.32,
		Price:  205.53,
		Volume: 15957632,
	}
)

func Test_toCandles(t *testing.T) {
	cases := []struct {
		data    Data
		want    []data.Candle
		wantErr bool
	}{
		{
			data: streamData,
			want: []data.Candle{
				goldenCandle,
				goldenCandle,
				goldenCandle,
			},
			wantErr: false,
		},
		{
			data:    nil,
			want:    []data.Candle{},
			wantErr: true,
		},
	}

	for _, tc := range cases {
		var mockData MockData
		mockData, err := mockData.unmarshal(tc.data)
		if tc.wantErr {
			assert.NotNil(t, err)
		} else {
			assert.Nil(t, err)
		}

		actual := mockData.toCandles()
		assert.Equal(t, tc.want, actual)
	}
}

func Test_GetCandleStream(t *testing.T) {

	cases := []struct {
		data       Data
		numCandles int
		wantErr    bool
	}{
		{
			data:       streamData,
			numCandles: 3,
			wantErr:    false,
		},
		{
			data:       nil,
			numCandles: 0,
			wantErr:    true,
		},
	}

	for _, tc := range cases {
		wg := &sync.WaitGroup{}
		ctx := context.Background()
		mocker := NewMocker(wg, tc.data)

		wg.Add(1)
		stream, _, err := mocker.Stream(ctx, 1*time.Hour, "AAPL", "1h")
		if !tc.wantErr {
			assert.Nil(t, err)
		} else {
			assert.NotNil(t, err)
		}
		wg.Wait()

		timer := time.NewTimer(5 * time.Millisecond) // wait long enough to read off stream
		go func() {
			<-timer.C
			close(stream)
		}()

		i := 0
		for range stream {
			i++
		}

		assert.Equal(t, tc.numCandles, i)
	}
}
