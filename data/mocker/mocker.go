package mocker

import (
	"context"
	"encoding/json"
	"sync"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/definitelynotagoat/gotrade/data"
)

// Mocker is a data stream mocker for test purposes
type Mocker struct {
	data Data
	wg   *sync.WaitGroup
}

//MockData is the struct representation of curl https://cloud.iexapis.com/stable/stock/aapl/chart/1y\?token\=?
type MockData []struct {
	Date           string  `json:"date"`
	Open           float64 `json:"open"`
	Close          float64 `json:"close"`
	High           float64 `json:"high"`
	Low            float64 `json:"low"`
	Volume         int     `json:"volume"`
	UOpen          float64 `json:"uOpen"`
	UClose         float64 `json:"uClose"`
	UHigh          float64 `json:"uHigh"`
	ULow           float64 `json:"uLow"`
	UVolume        int     `json:"uVolume"`
	Change         float64 `json:"change"`
	ChangePercent  float64 `json:"changePercent"`
	Label          string  `json:"label"`
	ChangeOverTime float64 `json:"changeOverTime"`
}

// NewMocker returns a new mocker
func NewMocker(wg *sync.WaitGroup, data Data) *Mocker {
	return &Mocker{
		data: data,
		wg:   wg,
	}
}

// Stream will start a mock candle stream and return it
func (m *Mocker) Stream(ctx context.Context, interval time.Duration, symbol, durstr string) (chan data.Candle, []string, error) {
	var mockData MockData
	stream := make(chan data.Candle, 5000) // high on purpose so we can buffer in our tests
	mockData, err := mockData.unmarshal(m.data)
	if err != nil {
		m.wg.Done()
		return stream, nil, errors.Wrap(err, "could not unmarshal candle data")
	}
	candles := mockData.toCandles()
	m.startCandleStream(stream, candles)

	return stream, nil, nil
}

// startCandleStream starts the mock candle stream
func (m *Mocker) startCandleStream(stream chan data.Candle, candles []data.Candle) {
	go func() {
		for _, candle := range candles {
			stream <- candle
		}
		m.wg.Done()
	}()
}

// converts MockData to []data.Candle
func (m *MockData) toCandles() []data.Candle {
	candles := []data.Candle{}
	for _, mock := range *m {
		candles = append(
			candles,
			data.Candle{
				Date:   mock.Date,
				Open:   mock.Open,
				Close:  mock.Close,
				High:   mock.High,
				Low:    mock.Low,
				Price:  mock.Close,
				Volume: mock.Volume,
			},
		)
	}

	return candles
}

// unmarshal unmarshals mock data into a struct
func (m *MockData) unmarshal(v []byte) (MockData, error) {
	var mockData MockData
	err := json.Unmarshal(v, &mockData)
	if err != nil {
		return mockData, errors.Wrap(err, "could not unmarshal mock data")
	}

	return mockData, nil
}
