package cryptowatch

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_Prime(t *testing.T) {
	primer := NewPrimer("BTC-USD", "coinbase-pro")
	err := primer.Prime()
	assert.Nil(t, err)
}
