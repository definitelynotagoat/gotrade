package cryptowatch

import (
	"fmt"

	cw "code.cryptowat.ch/cw-sdk-go/client/rest"
)

type Primer struct {
	client   *cw.CWRESTClient
	symbol   string
	exchange string
}

func NewPrimer(symbol, exchange string) *Primer {
	client := cw.NewCWRESTClient(nil)

	return &Primer{
		client:   client,
		symbol:   symbol,
		exchange: exchange,
	}
}

func (p *Primer) Prime() error {
	m, err := p.client.GetOHLC(
		p.exchange,
		p.symbol,
	)

	if err != nil {
		return err
	}

	fmt.Println(m)
	return nil
}
