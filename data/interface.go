package data

import (
	"context"
	"time"
)

type Candle struct {
	Symbol string  `json:"symbol"`
	Date   string  `json:"date"`
	Open   float64 `json:"open"`
	Close  float64 `json:"close"`
	High   float64 `json:"high"`
	Low    float64 `json:"low"`
	Price  float64 `json:"price"`
	Volume float64 `json:"volume"`
}

type DataStreamer interface {
	Prime(symbol string, interval int) (chan Candle, error)
	Stream(ctx context.Context, stream chan Candle, interval time.Duration, symbol string) ([]string, error)
}
