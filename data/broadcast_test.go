package data

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

func Test_Start(t *testing.T) {
	logger := zap.NewNop()
	cases := []struct {
		send      int
		receivers int
		wantLen   int
	}{
		{
			send:      3,
			receivers: 5,
			wantLen:   3,
		},
		{
			send:      4,
			receivers: 7,
			wantLen:   4,
		},
	}

	for _, tc := range cases {
		stream := make(chan Candle, 10)

		i := 0
		var receivers []chan Candle
		var lengths []*int

		for i < tc.receivers {
			receiver := make(chan Candle, 10)

			go func(chan Candle) {
				count := 0
				for range receiver {
					count++
				}
				lengths = append(lengths, &count)
			}(receiver)

			receivers = append(receivers, receiver)
			i++
		}

		ctx := context.Background()
		broadcast := NewBroadcast(stream, logger, receivers...)
		broadcast.Start(ctx)

		for tc.send != 0 {
			stream <- Candle{}
			tc.send--
		}

		timer := time.NewTimer(5 * time.Millisecond)
		<-timer.C

		for _, l := range lengths {
			assert.Equal(t, tc.wantLen, *l)
		}
	}
}
