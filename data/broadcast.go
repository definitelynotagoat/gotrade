package data

import (
	"context"
	"fmt"

	"go.uber.org/zap"
)

// Broadcast is a multiplexer for candle streams
type Broadcast struct {
	stream    chan Candle
	receivers []chan Candle
	logger    *zap.Logger
}

// NewBroadcast returns a pointer to a Broadcast
func NewBroadcast(stream chan Candle, logger *zap.Logger, receivers ...chan Candle) *Broadcast {
	return &Broadcast{
		stream:    stream,
		receivers: receivers,
		logger:    logger,
	}
}

// AddReceiver adds a receiver to the broadcast list
func (b *Broadcast) AddReceiver(receiver chan Candle) {
	b.receivers = append(b.receivers, receiver)
}

// Start listens off a broadcast stream and redirects the value to all receivers
func (b *Broadcast) Start(ctx context.Context) {
	go func() {
		for candle := range b.stream {
			b.logger.Debug(
				"received candle to broadcast",
				zap.String("candle", fmt.Sprintf("%v", candle)),
			)
			for _, receiver := range b.receivers {
				receiver <- candle
			}
		}
	}()
}
