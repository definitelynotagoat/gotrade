package coinbasepro

import (
	"fmt"
	"net/http"
	"strconv"
	"sync"
	"time"

	ws "github.com/gorilla/websocket"
	"github.com/pkg/errors"
	"github.com/preichenberger/go-coinbasepro/v2"
	"gitlab.com/definitelynotagoat/gotrade/data"
	"go.uber.org/zap"
)

// Feed is the representation of the websocket coinbasepro feed
type Feed interface {
	Subscribe(symbol string) error
	Start()
	SetPrice(msg coinbasepro.Message) error
	GetPrice(symbol string) (data.Candle, error)
}

// Conn represents functions needed from ws.Conn
type Conn interface {
	ReadJSON(v interface{}) error
	WriteJSON(v interface{}) error
}

// Dialer represents functions needed from ws.Dialer
type Dialer interface {
	Dial(urlStr string, requestHeader http.Header) (Conn, *http.Response, error)
}

// DialWrap wraps the ws.Dialer to satisfy the dialer interface
type DialWrap struct {
	Dialer *ws.Dialer
}

// Dial wraps ws.Dialer.Dial to satisfy the dialer interface
func (d DialWrap) Dial(urlStr string, requestHeader http.Header) (Conn, *http.Response, error) {
	return d.Dialer.Dial(urlStr, requestHeader)
}

// WebSocketFeed is a price feed driven by coinbase pros websocket api.
// The object contains the latest price.
type WebSocketFeed struct {
	data   map[string]data.Candle
	mu     *sync.Mutex
	wsConn Conn
	dialer Dialer
	logger *zap.Logger
}

// NewWebSocketFeed returns a new WebSocketFeed
func NewWebSocketFeed(dialer Dialer, logger *zap.Logger) (*WebSocketFeed, error) {
	mu := &sync.Mutex{}
	data := make(map[string]data.Candle)

	wsConn, _, err := dialer.Dial("wss://ws-feed.pro.coinbase.com", nil)
	if err != nil {
		logger.Error(
			"could not start coinbase websocket",
			zap.String("error", err.Error()),
		)
		return nil, errors.Wrap(err, "could not start coinbase websocket")
	}

	feed := &WebSocketFeed{
		mu:     mu,
		logger: logger,
		data:   data,
		wsConn: wsConn,
		dialer: dialer,
	}

	feed.Start()
	return feed, nil
}

// Subscribe aggregates a ticker websocket feed and updates the latest price in the data map
func (w *WebSocketFeed) Subscribe(symbol string) error {
	subscribe := coinbasepro.Message{
		Type: "subscribe",
		Channels: []coinbasepro.MessageChannel{
			coinbasepro.MessageChannel{
				Name: "ticker",
				ProductIds: []string{
					symbol,
				},
			},
		},
	}

	err := w.wsConn.WriteJSON(subscribe)
	if err != nil {
		w.logger.Info(
			"could not write subscribe to coinbase websocket",
			zap.String("error", err.Error()),
		)
		return errors.Wrap(err, "could not start coinbase websocket")
	}
	w.logger.Info(
		"successfully started subscribe websocket",
		zap.String("symbol", symbol),
	)

	return nil
}

func (w *WebSocketFeed) Start() {
	go func() {
		for true {
			message := coinbasepro.Message{}
			err := w.wsConn.ReadJSON(&message)
			if err != nil {
				w.logger.Info(
					"could not read json off websocket",
					zap.String("error", err.Error()),
				)
			} else {
				if message.Type == "error" {
					w.logger.Info(
						"received error message from websocket",
						zap.String("error", message.Message),
					)
				} else {
					if message.ProductID != "" {
						w.SetPrice(message)
					}
				}
			}
		}
	}()
}

// SetPrice sets the current price of the WebSocketFeed
func (w *WebSocketFeed) SetPrice(msg coinbasepro.Message) error {
	price, err := strconv.ParseFloat(msg.Price, 64)
	if err != nil {
		return errors.Wrap(err, "could not convert coinbase message price to float")
	}
	w.mu.Lock()
	w.data[msg.ProductID] = data.Candle{
		Symbol: msg.ProductID,
		Price:  price,
		Date:   time.Time(msg.Time).String(),
	}
	w.mu.Unlock()
	return nil
}

// GetPrice returns the latest price from the websocket feed
func (w *WebSocketFeed) GetPrice(symbol string) (data.Candle, error) {
	w.mu.Lock()
	c, ok := w.data[symbol]
	if !ok {
		return data.Candle{}, fmt.Errorf("could not find %s in data feed", symbol)
	}
	w.mu.Unlock()
	return c, nil
}
