package coinbasepro

import (
	"context"
	"fmt"
	"time"

	"github.com/pkg/errors"
	"github.com/preichenberger/go-coinbasepro/v2"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/rs/xid"
	"gitlab.com/definitelynotagoat/gotrade/data"
	"go.uber.org/zap"
)

const (
	namespace = "gotrade"
)

type CoinbaseDuration int

const (
	Minute         CoinbaseDuration = 60
	FiveMinutes    CoinbaseDuration = 300
	FifteenMinutes CoinbaseDuration = 900
	Hour           CoinbaseDuration = 3600
	SixHours       CoinbaseDuration = 21600
	Day            CoinbaseDuration = 86400
)

// Streamer represents a coinbasepro Streamer
type Streamer struct {
	logger *zap.Logger
	feed   Feed
	client *coinbasepro.Client
}

// NewStreamer returns a new coinbasepro NewStreamer
func NewStreamer(feed Feed, logger *zap.Logger) *Streamer {
	return &Streamer{
		logger: logger,
		feed:   feed,
		client: coinbasepro.NewClient(),
	}
}

type StreamParams struct {
	Interval time.Duration
}

// Stream starts a datastream for coinbase pro
func (s *Streamer) Stream(ctx context.Context, stream chan data.Candle, interval time.Duration, symbol string) ([]string, error) {
	ticker := time.NewTicker(interval)
	err := s.feed.Subscribe(symbol)
	if err != nil {
		return nil, errors.Wrap(err, "could not start stream")
	}

	id := xid.New().String()

	chartName := fmt.Sprintf("chart_%s_%s", interval.String(), id)
	chartmetrics := prometheus.NewGauge(
		prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      chartName,
			Help: fmt.Sprintf(
				"Chart for %s on an interval of %s",
				symbol,
				interval.String(),
			),
		},
	)
	prometheus.MustRegister(chartmetrics)

	go func() {
		for range ticker.C {
			// TODO: check if the time is delayed off the feed
			candle, err := s.feed.GetPrice(symbol)
			if err != nil {
				s.logger.Info(
					"could not receive candle off feed",
					zap.String("error", err.Error()),
					zap.String("symbol", symbol),
					zap.String("interval", interval.String()),
				)
			} else {
				s.logger.Debug(
					"received candle off feed",
					zap.String("time", candle.Date),
					zap.Float64("price", candle.Price),
					zap.String("symbol", candle.Symbol),
					zap.String("interval", interval.String()),
				)
				chartmetrics.Set(candle.Price)
				stream <- candle
			}
		}
	}()

	return []string{chartName}, nil
}

// Prime feeds a datastream but uses historical data at an interval
func (s *Streamer) Prime(symbol string, interval int) (chan data.Candle, error) {
	s.logger.Debug(
		"starting primer",
		zap.Int("interval", interval),
	)

	stream := make(chan data.Candle, 500)

	params, err := GetHistoricRatesParams(interval)
	if err != nil {
		return stream, err
	}

	rates, err := s.client.GetHistoricRates(symbol, params)
	if err != nil {
		return stream, errors.Wrap(err, "could not get coinbasepro historical rate")
	}

	for _, rate := range rates {
		stream <- data.Candle{
			Symbol: symbol,
			Date:   rate.Time.String(),
			Open:   rate.Open,
			Close:  rate.Close,
			Price:  rate.Close,
			High:   rate.High,
			Low:    rate.Low,
			Volume: rate.Volume,
		}
	}

	return stream, nil
}

func GetHistoricRatesParams(interval int) (coinbasepro.GetHistoricRatesParams, error) {
	end := time.Now()
	limit := 300
	switch interval {
	case int(Minute):
		start := end.Add(time.Duration(-limit) * time.Minute)
		return coinbasepro.GetHistoricRatesParams{
			Start:       start,
			End:         end,
			Granularity: interval,
		}, nil
	case int(FifteenMinutes):
		start := end.Add(time.Duration(-limit*15) * time.Minute)
		return coinbasepro.GetHistoricRatesParams{
			Start:       start,
			End:         end,
			Granularity: interval,
		}, nil
	case int(Hour):
		start := end.Add(time.Duration(-limit) * time.Hour)
		return coinbasepro.GetHistoricRatesParams{
			Start:       start,
			End:         end,
			Granularity: interval,
		}, nil
	case int(SixHours):
		start := end.Add(time.Duration(-limit*6) * time.Hour)
		return coinbasepro.GetHistoricRatesParams{
			Start:       start,
			End:         end,
			Granularity: interval,
		}, nil
	case int(Day):
		start := end.Add(time.Duration(-limit*24) * time.Hour)
		return coinbasepro.GetHistoricRatesParams{
			Start:       start,
			End:         end,
			Granularity: interval,
		}, nil
	default:
		return coinbasepro.GetHistoricRatesParams{}, fmt.Errorf("invalid interval %d", interval)
	}
}
