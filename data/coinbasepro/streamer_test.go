package coinbasepro

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/preichenberger/go-coinbasepro/v2"
	"github.com/stretchr/testify/assert"
	"gitlab.com/definitelynotagoat/gotrade/data"
	"go.uber.org/zap"
)

type MockFeed struct {
	wantErr bool
	candles []data.Candle
}

func (m *MockFeed) Subscribe(symbol string) error {
	return nil
}

func (m *MockFeed) Start() {}

func (m *MockFeed) SetPrice(msg coinbasepro.Message) error {
	return nil
}

func (m *MockFeed) GetPrice(symbol string) (data.Candle, error) {
	if m.wantErr {
		return data.Candle{}, errors.New("Get price error")
	}

	var candle data.Candle
	if len(m.candles) > 0 {
		candle, m.candles = m.candles[len(m.candles)-1], m.candles[:len(m.candles)-1]
	}
	return candle, nil
}

func Test_Stream(t *testing.T) {
	logger := zap.NewNop()

	cases := []struct {
		name      string
		feed      Feed
		wantPrice float64
		wantErr   bool
	}{
		{
			name: "successful",
			feed: &MockFeed{
				wantErr: false,
				candles: []data.Candle{
					data.Candle{
						Price: 10000,
					},
					data.Candle{
						Price: 9000,
					},
					data.Candle{
						Price: 8000,
					},
				},
			},
			wantPrice: 8000,
			wantErr:   false,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			ctx := context.Background()
			streamer := NewStreamer(tc.feed, logger)
			stream, _, err := streamer.Stream(ctx, 1*time.Millisecond, "BTC-USD")
			if tc.wantErr {
				assert.NotNil(t, err)
			} else {
				assert.Nil(t, err)
				timer := time.NewTimer(5 * time.Millisecond) // wait long enough to read off stream
				<-timer.C
				candle := <-stream

				assert.Equal(t, tc.wantPrice, candle.Price)
			}
		})
	}
}
