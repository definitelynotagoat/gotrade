package coinbasepro

import (
	"errors"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

type MockDialer struct {
	reads        []interface{}
	wantDialErr  bool
	wantReadErr  bool
	wantWriteErr bool
}

func (m *MockDialer) Dial(urlStr string, requestHeader http.Header) (Conn, *http.Response, error) {
	conn := MockConnection{
		reads:        m.reads,
		wantReadErr:  m.wantReadErr,
		wantWriteErr: m.wantWriteErr,
	}

	resp := &http.Response{}

	if m.wantDialErr {
		return &conn, resp, errors.New("mock dial error")
	}

	return &conn, resp, nil
}

type MockConnection struct {
	reads        []interface{}
	wantReadErr  bool
	wantWriteErr bool
}

func (m *MockConnection) ReadJSON(v interface{}) error {
	if m.wantReadErr {
		return errors.New("mock read error")
	}

	if len(m.reads) > 0 {
		var item interface{}
		item, m.reads = m.reads[len(m.reads)-1], m.reads[:len(m.reads)-1]
		v = item
	}

	return nil
}

func (m *MockConnection) WriteJSON(v interface{}) error {
	if m.wantWriteErr {
		return errors.New("mock write error")
	}

	return nil
}

func Test_NewWebSocketFeed(t *testing.T) {
	logger := zap.NewNop()

	cases := []struct {
		name     string
		dialer   Dialer
		wantErr  bool
		wantFeed *WebSocketFeed
	}{
		{
			name: "successful",
			dialer: &MockDialer{
				wantDialErr: false,
			},
			wantErr: false,
		},
		{
			name: "unsuccessful",
			dialer: &MockDialer{
				wantDialErr: true,
			},
			wantErr: true,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			_, err := NewWebSocketFeed(tc.dialer, logger)
			if tc.wantErr {
				assert.NotNil(t, err)
			} else {
				assert.Nil(t, err)
			}
		})
	}
}

func Test_Subscribe(t *testing.T) {
	logger := zap.NewNop()

	cases := []struct {
		name     string
		dialer   Dialer
		wantErr  bool
		wantFeed *WebSocketFeed
	}{
		{
			name: "successful",
			dialer: &MockDialer{
				wantDialErr:  false,
				wantWriteErr: false,
			},
			wantErr: false,
		},
		{
			name: "unsuccessful",
			dialer: &MockDialer{
				wantDialErr:  false,
				wantWriteErr: true,
			},
			wantErr: true,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			feed, err := NewWebSocketFeed(tc.dialer, logger)
			assert.Nil(t, err)
			err = feed.Subscribe("BTC-USD")
			if tc.wantErr {
				assert.NotNil(t, err)
			} else {
				assert.Nil(t, err)
			}
		})
	}
}

// func Test_Start(t *testing.T) {
// 	logger := zap.NewNop()

// 	cases := []struct {
// 		name       string
// 		dialer     Dialer
// 		wantCandle data.Candle
// 	}{
// 		{
// 			name: "successful",
// 			dialer: &MockDialer{
// 				wantDialErr: false,
// 				reads: []interface{}{
// 					coinbasepro.Message{
// 						ProductID: "BTC-USD",
// 						Price:     "8000",
// 					},
// 					coinbasepro.Message{
// 						ProductID: "BTC-USD",
// 						Price:     "8001",
// 					},
// 					coinbasepro.Message{
// 						ProductID: "BTC-USD",
// 						Price:     "7999",
// 					},
// 				},
// 				wantWriteErr: false,
// 			},
// 			wantCandle: data.Candle{
// 				Price:  8000,
// 				Symbol: "BTC-USD",
// 			},
// 		},
// 		// {
// 		// 	name: "unsuccessful",
// 		// 	dialer: &MockDialer{
// 		// 		wantDialErr: false,
// 		// 		wantReadErr: false,
// 		// 		reads: []interface{}{
// 		// 			coinbasepro.Message{
// 		// 				ProductID: "BTC-USD",
// 		// 				Price:     "8000",
// 		// 			},
// 		// 			coinbasepro.Message{
// 		// 				ProductID: "BTC-USD",
// 		// 				Price:     "8001",
// 		// 			},
// 		// 			coinbasepro.Message{
// 		// 				ProductID: "BTC-USD",
// 		// 				Price:     "7999",
// 		// 			},
// 		// 		},
// 		// 	},
// 		// },
// 	}

// 	for _, tc := range cases {
// 		t.Run(tc.name, func(t *testing.T) {
// 			feed, err := NewWebSocketFeed(tc.dialer, logger)
// 			assert.Nil(t, err)
// 			err = feed.Subscribe("BTC-USD")
// 			assert.Nil(t, err)
// 			time.Sleep(500 * time.Millisecond)
// 			candle, err := feed.GetPrice("BTC-USD")
// 			assert.NotNil(t, err)
// 			assert.Equal(t, tc.wantCandle, candle)
// 		})
// 	}
// }
