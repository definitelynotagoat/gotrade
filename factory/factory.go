package factory

import (
	"context"
	"fmt"
	"time"

	"github.com/pkg/errors"
	"github.com/rs/xid"
	"gitlab.com/definitelynotagoat/gotrade/api/strats/collector"
	"gitlab.com/definitelynotagoat/gotrade/data"
	"gitlab.com/definitelynotagoat/gotrade/strats"
	"gitlab.com/definitelynotagoat/gotrade/trader/paper"

	"encoding/json"

	"go.uber.org/zap"
)

/*
The factory package is a factory driver to connect
a data stream at all supported intervals, to all supported
strategies to all supported paper traders.
*/

type Factory struct {
	logger       *zap.Logger
	streamer     data.DataStreamer
	strategies   []strats.Strategy
	apiCollector *collector.Collector
	symbol       string
	capital      float64
}

var intervals = []time.Duration{
	time.Minute,
	15 * time.Minute,
	time.Hour,
	4 * time.Hour,
	24 * time.Hour,
}

var intervalsStr = []string{
	"1m",
	"15m",
	"1h",
	"6h",
	"24h",
}

func NewFactory(symbol string, capital float64, streamer data.DataStreamer, strategies []strats.Strategy, apiCollector *collector.Collector, logger *zap.Logger) *Factory {
	return &Factory{
		logger:       logger,
		streamer:     streamer,
		strategies:   strategies,
		capital:      capital,
		apiCollector: apiCollector,
		symbol:       symbol,
	}
}

func (f *Factory) StartFactories(ctx context.Context) error {
	f.logger.Info("starting factories")

	for i, duration := range intervals {
		stream := make(chan data.Candle, 1000)
		chartMetrics, err := f.streamer.Stream(ctx, stream, duration, f.symbol)
		if err != nil {
			return errors.Wrap(err, "could not start data stream")
		}

		broadcast := data.NewBroadcast(stream, f.logger)
		broadcast.Start(ctx)

		for _, strategy := range f.strategies {

			localStream := make(chan data.Candle, 100)
			broadcast.AddReceiver(localStream)

			id := fmt.Sprintf("%s_%s_%s", duration.String(), intervalsStr[i], xid.New())
			sigs, stratMetrics := strategy.Start(ctx, duration, localStream)

			trader := paper.NewTrader(f.capital, strategy.GetID(), f.logger)
			capitalCh, metrics := trader.Start(ctx, sigs, duration)
			metrics = append(metrics, stratMetrics...)
			metrics = append(metrics, chartMetrics...)

			go func(strategy strats.Strategy) {
				for c := range capitalCh {
					f.apiCollector.UpdateStrategy(id, collector.Strategy{
						MetricsIDs: metrics,
						Strategy:   strategy.GetType(),
						Capital:    c,
						Params:     json.RawMessage(strategy.GetParams()),
					})
				}
			}(strategy)
		}
	}

	return nil
}
