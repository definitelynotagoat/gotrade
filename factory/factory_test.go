package factory

import (
	"context"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/definitelynotagoat/gotrade/api/strats/collector"
	"gitlab.com/definitelynotagoat/gotrade/data/mocker"
	"gitlab.com/definitelynotagoat/gotrade/strats"
	"gitlab.com/definitelynotagoat/gotrade/strats/rsi"
	"go.uber.org/zap"
)

func Test_StartFactiories(t *testing.T) {
	logger := zap.NewNop()

	cases := []struct {
		strats   int
		expected int
	}{
		{
			strats:   4,
			expected: 20,
		},
		{
			strats:   19,
			expected: 95,
		},
		{
			strats:   0,
			expected: 0,
		},
	}

	for _, tc := range cases {

		wg := &sync.WaitGroup{}
		ctx := context.Background()
		c := collector.NewCollector(10000, logger)

		mockstream := mocker.NewMocker(wg, mocker.ONEMONTH)

		var strategies []strats.Strategy
		for tc.strats > 0 {
			s := rsi.NewStrategy(10, 90, 20, logger)
			strategies = append(strategies, s)
			tc.strats--
		}

		f := NewFactory("AAPL", 10000, mockstream, strategies, c, logger)

		wg.Add(5) // 5 intervals
		err := f.StartFactories(ctx)
		wg.Wait()
		assert.Nil(t, err)

		time.Sleep(100 * time.Millisecond) //wait for all channels to receive

		assert.Equal(t, tc.expected, len(c.GetStrategies()))

	}

}
