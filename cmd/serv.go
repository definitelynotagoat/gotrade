package cmd

import (
	"context"
	"net/http"
	"time"

	"gitlab.com/definitelynotagoat/gotrade/api/strats/collector"
	servconf "gitlab.com/definitelynotagoat/gotrade/conf"
	"gitlab.com/definitelynotagoat/gotrade/data"
	"gitlab.com/definitelynotagoat/gotrade/data/coinbasepro"

	"go.uber.org/zap"

	"github.com/gorilla/mux"
	ws "github.com/gorilla/websocket"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/cobra"
	statsapi "gitlab.com/definitelynotagoat/gotrade/api/strats"
	"gitlab.com/definitelynotagoat/gotrade/factory"
)

func newservCommand() *cobra.Command {
	var (
		conf   string
		debug  bool
		listen string
	)

	var test = &cobra.Command{
		Use:   "serv",
		Short: "serv runs all known strategies and exposes an api for winning strategies",
		Run: func(cmd *cobra.Command, args []string) {
			serv := make(chan int)
			ctx := context.Background()

			var logger *zap.Logger
			if debug {
				logger, _ = zap.NewDevelopment()
			} else {
				logger, _ = zap.NewProduction()
			}

			defer logger.Sync() // flushes buffer, if any

			if conf == "" {
				logger.Fatal("cannot start server without conf file")
			}

			config, err := servconf.NewServer(conf, logger)
			if err != nil {
				logger.Fatal(
					"cannot start server bad config",
					zap.String("error", err.Error()),
				)
			}

			apiCollector := collector.NewCollector(config.Capital, 15*time.Second, logger)
			startHTTPServer(listen, logger, apiCollector)

			strats, err := config.GetStrats()
			if err != nil {
				logger.Fatal(
					"could not load strategies",
					zap.Error(err),
				)
			}

			var streamer data.DataStreamer
			switch config.Name {
			case servconf.COINBASE:
				wsDialer := coinbasepro.DialWrap{
					Dialer: &ws.Dialer{},
				}
				feed, err := coinbasepro.NewWebSocketFeed(&wsDialer, logger)
				if err != nil {
					logger.Fatal("could not start websocket connection to coinbasepro")
				}
				streamer = coinbasepro.NewStreamer(feed, logger)
			// case servconf.IEX:
			// 	api, err := config.GetIEXAPI()
			// 	if err != nil {
			// 		logger.Fatal(
			// 			"cannot start iex streamer",
			// 			zap.String("error", err.Error()),
			// 		)
			// 	}
			// 	streamer = iexcloud.NewStreamer(&http.Client{}, api.Token, logger)
			default:
				logger.Fatal("no specified streamer")
			}

			for _, symbol := range config.Symbols {
				factories := factory.NewFactory(
					symbol,
					config.Capital,
					streamer,
					strats,
					apiCollector,
					logger,
				)
				err = factories.StartFactories(ctx)

				if err != nil {
					logger.Info(
						"could not start factories",
						zap.String("symbol", symbol),
						zap.Error(err),
					)
				}
			}

			<-serv
		},
	}

	test.PersistentFlags().StringVar(&conf, "conf", "", "server configuration file")
	test.PersistentFlags().StringVar(&listen, "listen", "127.0.0.1:8888", "listen address for the server")
	test.PersistentFlags().BoolVar(&debug, "debug", false, "turn on debug logging")

	return test
}

func startHTTPServer(listen string, logger *zap.Logger, apiCollector *collector.Collector) {
	go func() {
		stratsAPI := statsapi.NewStratAPI(apiCollector)

		r := mux.NewRouter()
		r.HandleFunc("/strats", stratsAPI.StratsReport)
		r.HandleFunc("/winning", stratsAPI.WinningStratReport)
		r.Handle("/metrics", promhttp.Handler())

		s := &http.Server{
			Addr:           listen,
			ReadTimeout:    8 * time.Second,
			WriteTimeout:   8 * time.Second,
			MaxHeaderBytes: 1 << 20,
			Handler:        r,
		}
		logger.Fatal(
			"could not start prometheus handler",
			zap.Error(s.ListenAndServe()),
		)
	}()
}
