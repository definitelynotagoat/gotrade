package cmd

import (
	"github.com/spf13/cobra"
)

func newRootCommand() *cobra.Command {
	rootCommand := &cobra.Command{
		Use:   "gotrade",
		Short: "Go Trade is an all in one trading bot written in go.",
	}

	rootCommand.AddCommand(
		newservCommand(),
	)

	return rootCommand
}

func Execute() error {
	return newRootCommand().Execute()
}
