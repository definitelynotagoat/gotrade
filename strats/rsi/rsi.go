package rsi

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"go.uber.org/zap"

	ta "github.com/markcheno/go-talib"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/rs/xid"

	"gitlab.com/definitelynotagoat/gotrade/data"
	"gitlab.com/definitelynotagoat/gotrade/strats"
)

const (
	namespace = "gotrade"
)

// Strategy is a basic RSI based trading pattern
type Strategy struct {
	period      int
	sellTrigger int
	buyTrigger  int
	id          string
	logger      *zap.Logger
	desc        string
}

// NewStrategy returns a new rsi based Strategy
func NewStrategy(period int, sellTrigger int, buyTrigger int, logger *zap.Logger) *Strategy {
	return &Strategy{
		period:      period,
		sellTrigger: sellTrigger,
		buyTrigger:  buyTrigger,
		logger:      logger,
		id:          xid.New().String(),
		desc:        "rsi",
	}
}

// ConfigureStrategies configures sane strategies, in this case 90 different RSI strats
func ConfigureStrategies(logger *zap.Logger) []strats.Strategy {
	var strategies []strats.Strategy
	for x := 2; x < 20; x++ {
		for i := 5; i < 100; i += 5 {
			buysig := i
			sellsig := 100 - i
			if buysig < sellsig {
				strat := NewStrategy(x, sellsig, buysig, logger)
				strategies = append(strategies, strat)
			} else {
				break
			}
		}
	}

	return strategies
}

// Start starts an rsi strategy over a stream of data.
func (s *Strategy) Start(ctx context.Context, interval time.Duration, stream chan data.Candle) (chan strats.Signal, []string) {
	sigs := make(chan strats.Signal, 100)

	rsiName := fmt.Sprintf("rsi_%s_%s", interval.String(), s.id)
	rsimetrics := prometheus.NewGauge(
		prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      rsiName,
			Help: fmt.Sprintf(
				"RSI on a period of %d, buy signal of %d, sell signal of %d, and an interval of %s",
				s.period,
				s.buyTrigger,
				s.sellTrigger,
				interval.String(),
			),
		},
	)
	prometheus.MustRegister(rsimetrics)

	s.logger.Info(
		"starting strategy",
		zap.String("type", "rsi"),
		zap.String("id", s.id),
		zap.Int("period", s.period),
		zap.Int("sell_trigger", s.sellTrigger),
		zap.Int("buy_trigger", s.buyTrigger),
		zap.String("interval", interval.String()),
	)

	go func() {
		// if the context signals quit,
		// close the buying and selling channels
		go func() {
			<-ctx.Done()
			close(sigs)
		}()

		//grab enough candles off the data stream to start calculating RSI
		queue, lastCandle := s.initQueue(stream)

		//init RSI
		rsi := ta.Rsi(queue, s.period)
		s.logger.Debug(
			"computed strategy",
			zap.String("type", "rsi"),
			zap.String("id", s.id),
			zap.Int("period", s.period),
			zap.Int("sell_trigger", s.sellTrigger),
			zap.Int("buy_trigger", s.buyTrigger),
			zap.String("interval", interval.String()),
			zap.Float64("rsi", rsi[s.period]),
			zap.String("rsi_array", fmt.Sprintf("%v", rsi)),
			zap.String("queue", fmt.Sprintf("%v", queue)),
		)
		rsimetrics.Set(rsi[s.period])
		s.evaluateSignals(rsi, lastCandle, interval, sigs)

		go func() {
			for candle := range stream {
				queue = queue[1:]                   // pop the queue
				queue = append(queue, candle.Price) // push the new candle
				rsi := ta.Rsi(queue, s.period)
				rsimetrics.Set(rsi[s.period])
				s.logger.Debug(
					"computed strategy",
					zap.String("type", "rsi"),
					zap.String("id", s.id),
					zap.Int("period", s.period),
					zap.Int("sell_trigger", s.sellTrigger),
					zap.Int("buy_trigger", s.buyTrigger),
					zap.String("interval", interval.String()),
					zap.Float64("rsi", rsi[s.period]),
					zap.String("rsi_array", fmt.Sprintf("%v", rsi)),
					zap.String("queue", fmt.Sprintf("%v", queue)),
				)
				s.evaluateSignals(rsi, candle, interval, sigs)
			}
		}()
	}()

	return sigs, []string{rsiName}
}

// GetParams returns the strat params in raw json
func (s *Strategy) GetParams() []byte {
	return json.RawMessage(fmt.Sprintf(
		`{"period": %d, "buy_signal": %d, "sell_signal": %d }`,
		s.period,
		s.buyTrigger,
		s.sellTrigger,
	),
	)
}

// GetType returns the strategy type
func (s *Strategy) GetType() string {
	return s.desc
}

// GetID returns the strategy id
func (s *Strategy) GetID() string {
	return s.id
}

func (s *Strategy) initQueue(stream chan data.Candle) ([]float64, data.Candle) {
	queue := []float64{}
	var lastCandle data.Candle
	for candle := range stream {
		queue = append(queue, candle.Price)
		if len(queue) == (s.period + 1) {
			lastCandle = candle
			break
		}
	}

	return queue, lastCandle
}

func (s *Strategy) evaluateSignals(rsi []float64, candle data.Candle, interval time.Duration, sig chan strats.Signal) {
	if rsi[s.period] <= float64(s.buyTrigger) && rsi[s.period] != 0 {
		sig <- strats.Signal{
			Price:  candle.Price,
			Symbol: candle.Symbol,
			Type:   strats.BUY,
		}
		s.logSignal(true, rsi[s.period], interval)
	} else if rsi[s.period] >= float64(s.sellTrigger) {
		sig <- strats.Signal{
			Price:  candle.Price,
			Symbol: candle.Symbol,
			Type:   strats.SELL,
		}
		s.logSignal(false, rsi[s.period], interval)
	}
}

func (s *Strategy) logSignal(buy bool, rsi float64, interval time.Duration) {
	var msg string
	if buy {
		msg = "sent buy signal"
	} else {
		msg = "sent sell signal"
	}

	s.logger.Info(
		msg,
		zap.String("type", "rsi"),
		zap.String("id", s.id),
		zap.Int("period", s.period),
		zap.Int("sell_trigger", s.sellTrigger),
		zap.Int("buy_trigger", s.buyTrigger),
		zap.String("interval", interval.String()),
		zap.Float64("rsi", rsi),
	)
}
