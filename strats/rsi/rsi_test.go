package rsi

import (
	"context"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/definitelynotagoat/gotrade/data"
	"gitlab.com/definitelynotagoat/gotrade/data/mocker"
	"gitlab.com/definitelynotagoat/gotrade/strats"
	"go.uber.org/zap"
)

func Test_initQueue(t *testing.T) {
	logger := zap.NewNop()

	type want struct {
		queue []float64
		last  data.Candle
	}

	type args struct {
		data   mocker.Data
		period int
	}

	cases := []struct {
		name string
		args args
		want want
	}{
		{
			name: "period 2",
			args: args{
				data:   goldenStreamData,
				period: 2,
			},
			want: want{
				queue: []float64{
					goldenCandles[0].Close,
					goldenCandles[1].Close,
					goldenCandles[2].Close,
				},
				last: goldenCandles[2],
			},
		},
		{
			name: "period 7",
			args: args{
				data:   goldenStreamData,
				period: 7,
			},
			want: want{
				queue: []float64{
					goldenCandles[0].Close,
					goldenCandles[1].Close,
					goldenCandles[2].Close,
					goldenCandles[3].Close,
					goldenCandles[4].Close,
					goldenCandles[5].Close,
					goldenCandles[6].Close,
					goldenCandles[7].Close,
				},
				last: goldenCandles[7],
			},
		},
		{
			name: "period 14",
			args: args{
				data:   goldenStreamData,
				period: 14,
			},
			want: want{
				queue: []float64{
					goldenCandles[0].Close,
					goldenCandles[1].Close,
					goldenCandles[2].Close,
					goldenCandles[3].Close,
					goldenCandles[4].Close,
					goldenCandles[5].Close,
					goldenCandles[6].Close,
					goldenCandles[7].Close,
					goldenCandles[8].Close,
					goldenCandles[9].Close,
					goldenCandles[10].Close,
					goldenCandles[11].Close,
					goldenCandles[12].Close,
					goldenCandles[13].Close,
					goldenCandles[14].Close,
				},
				last: goldenCandles[14],
			},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			wg := &sync.WaitGroup{}
			ctx := context.Background()
			m := mocker.NewMocker(wg, tc.args.data)

			// wait for the data stream to fill up
			wg.Add(1)
			stream, _, err := m.Stream(ctx, 1*time.Hour, "AAPL", "1h")
			assert.Nil(t, err)
			wg.Wait()

			//buy/sell signals do not matter for this test
			strat := NewStrategy(tc.args.period, 90, 10, logger)
			queue, last := strat.initQueue(stream)

			assert.Equal(t, tc.want.queue, queue)
			assert.Equal(t, tc.want.last, last)
		})
	}
}

func Test_evaluateSignals(t *testing.T) {
	logger := zap.NewNop()

	type want struct {
		sellSigsNum int
		buySigsNum  int
	}

	type args struct {
		period  int
		rsi     []float64
		buySig  int
		sellSig int
	}

	cases := []struct {
		name string
		args args
		want want
	}{
		{
			name: "sell signal",
			args: args{
				period: 2,
				rsi: []float64{
					0,
					0,
					91,
				},
				buySig:  10,
				sellSig: 90,
			},
			want: want{
				buySigsNum:  0,
				sellSigsNum: 1,
			},
		},
		{
			name: "buy signal",
			args: args{
				period: 7,
				rsi: []float64{
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					8,
				},
				buySig:  10,
				sellSig: 90,
			},
			want: want{
				buySigsNum:  1,
				sellSigsNum: 0,
			},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			sigs := make(chan strats.Signal, 10)
			//buy/sell signals do not matter for this test
			strat := NewStrategy(tc.args.period, 90, 10, logger)
			strat.evaluateSignals(tc.args.rsi, data.Candle{}, 1*time.Hour, sigs)

			timer := time.NewTimer(5 * time.Millisecond)

			buys := 0
			sells := 0

			go func() {
				for sig := range sigs {
					if sig.Type == strats.BUY {
						buys++
					} else {
						sells++
					}
				}
			}()

			<-timer.C //waiting 5 millis to let the channel receive

			assert.Equal(t, tc.want.sellSigsNum, sells)
			assert.Equal(t, tc.want.buySigsNum, buys)
		})
	}
}

func Test_Start(t *testing.T) {
	logger := zap.NewNop()

	type want struct {
		sellSigsNum int
		buySigsNum  int
	}

	type args struct {
		data    mocker.Data
		period  int
		buySig  int
		sellSig int
	}

	cases := []struct {
		name string
		args args
		want want
	}{
		{
			name: "period 2, buy 30, sell 70",
			args: args{
				data:    goldenStreamData,
				period:  2,
				buySig:  30,
				sellSig: 70,
			},
			want: want{
				buySigsNum:  0,
				sellSigsNum: 7,
			},
		},
		{
			name: "period 7, buy 30, sell 70",
			args: args{
				data:    goldenStreamData,
				period:  7,
				buySig:  30,
				sellSig: 70,
			},
			want: want{
				buySigsNum:  0,
				sellSigsNum: 7,
			},
		},
		{
			name: "period 14, buy 30, sell 70",
			args: args{
				data:    goldenStreamData,
				period:  14,
				buySig:  30,
				sellSig: 70,
			},
			want: want{
				buySigsNum:  0,
				sellSigsNum: 1,
			},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			wg := &sync.WaitGroup{}
			ctx := context.Background()
			m := mocker.NewMocker(wg, tc.args.data)

			// wait for the data stream to fill up
			wg.Add(1)
			stream, _, err := m.Stream(ctx, 1*time.Hour, "AAPL", "1h")
			assert.Nil(t, err)
			wg.Wait()

			strat := NewStrategy(tc.args.period, tc.args.sellSig, tc.args.buySig, logger)
			sigs, _ := strat.Start(ctx, 1*time.Hour, stream)

			timer := time.NewTimer(20 * time.Millisecond)

			buys := 0
			sells := 0

			go func() {
				for sig := range sigs {
					if sig.Type == strats.BUY {
						buys++
					} else {
						sells++
					}
				}
			}()

			<-timer.C

			assert.Equal(t, tc.want.sellSigsNum, sells)
			assert.Equal(t, tc.want.buySigsNum, buys)
		})
	}
}

func Test_ConfigureStrategies(t *testing.T) {
	logger := zap.NewNop()
	cases := []struct {
		name    string
		wantLen int
	}{
		{
			name:    "Successful ConfigureStrategies",
			wantLen: 162,
		},
	}
	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			strats := ConfigureStrategies(logger)
			assert.Equal(t, tc.wantLen, len(strats))
		})
	}
}
