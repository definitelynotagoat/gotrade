# strats

This package contains all of our strategies. Each strategy will be signal based, either `buy` or `sell`. To understand how that works, each strategy must implement the following interface: 

```
type Strategy interface {
	Start(ctx context.Context, stream chan data.Candle) (chan int, chan int)
}
```
Specifically take note of the two int channels returned. When we start a strategy we want to return two buffered channels that represent the strategy notifying a buy or sell. 

Implementing the above interface will make our lives easier when we want to start piping strategies together to form more advanced signals. 