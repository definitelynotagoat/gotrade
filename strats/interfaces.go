package strats

import (
	"context"
	"time"

	"gitlab.com/definitelynotagoat/gotrade/data"
)

type StratID string
type SigType string

const (
	RSIStrategy StratID = "rsi_strategy"
	BUY         SigType = "BUY"
	SELL        SigType = "SELL"
)

type Signal struct {
	Symbol string
	Price  float64
	Type   SigType
}

type Strategy interface {
	Start(ctx context.Context, interval time.Duration, stream chan data.Candle) (chan Signal, []string)
	GetParams() []byte
	GetType() string
	GetID() string
}
