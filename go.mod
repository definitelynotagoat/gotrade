module gitlab.com/definitelynotagoat/gotrade

go 1.12

require (
	code.cryptowat.ch/cw-sdk-go v1.0.2
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/websocket v1.4.1
	github.com/markcheno/go-talib v0.0.0-20190307022042-cd53a9264d70
	github.com/pkg/errors v0.8.1
	github.com/preichenberger/go-coinbasepro/v2 v2.0.4
	github.com/prometheus/client_golang v1.1.0
	github.com/rs/xid v1.2.1
	github.com/spf13/cobra v0.0.5
	github.com/stretchr/testify v1.3.0
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.10.0
	golang.org/x/tools/gopls v0.1.7 // indirect
)
