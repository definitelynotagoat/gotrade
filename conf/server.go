package conf

import (
	"encoding/json"
	"io/ioutil"

	"github.com/pkg/errors"
	"gitlab.com/definitelynotagoat/gotrade/strats"
	"gitlab.com/definitelynotagoat/gotrade/strats/rsi"
	"go.uber.org/zap"
)

// Server represents the Servers configuration
type Server struct {
	Name    string      `json:"name"`
	API     interface{} `json:"api"`
	Symbols []string    `json:"symbols"`
	Capital float64     `json:"capital"`
	logger  *zap.Logger
	conf    string
}

// IEXAPI API auth
type IEXAPI struct {
	Token string `json:"token"`
}

// CoinbaseProAPI API auth
type CoinbaseProAPI struct{}

// Config represents the type of config
type Config string

const (
	// COINBASE Config
	COINBASE = "coinbasepro"
	// IEX Config
	IEX = "iex"
)

// NewServer returns a new server configuration
func NewServer(conf string, logger *zap.Logger) (*Server, error) {
	var server *Server
	f, err := ioutil.ReadFile(conf)
	if err != nil {
		return nil, errors.Wrap(err, "could not create server config")
	}
	server, err = server.unmarshal(f)
	if err != nil {
		return nil, errors.Wrap(err, "could not create server config")
	}

	server.conf = conf
	server.logger = logger

	return server, nil
}

// GetStrats gets all strategies to be used in a factory
func (s *Server) GetStrats() ([]strats.Strategy, error) {
	var strategies []strats.Strategy
	// Get all the rsi strategies
	strategies = append(strategies, rsi.ConfigureStrategies(s.logger)...)
	// TODO add more strats
	return strategies, nil
}

func (s *Server) unmarshal(v []byte) (*Server, error) {
	var server Server
	err := json.Unmarshal(v, &server)
	if err != nil {
		return nil, errors.Wrap(err, "could not unmarshal server")
	} else if server.Name == "" {
		return nil, errors.New("could not unmarshal empty server")
	}

	return &server, nil
}

// GetIEXAPI returns the API information needed for the IEX exchange
func (s *Server) GetIEXAPI() (IEXAPI, error) {
	var api IEXAPI
	if s.Name != IEX {
		return api, errors.New("cannot get IEXAPI from server config is not of type iex")
	}

	v, err := json.Marshal(s.API)
	if err != nil {
		return api, errors.Wrap(err, "cannot get IEXAPI bad bytes")
	}

	err = json.Unmarshal(v, &api)
	if err != nil {
		return api, errors.Wrap(err, "cannot get IEXAPI from server config")
	}

	return api, nil
}

// GetCoinbaseProAPI returns the API information needed for the Coinbasepro exchange
func (s *Server) GetCoinbaseProAPI() (CoinbaseProAPI, error) {
	var api CoinbaseProAPI
	if s.Name != COINBASE {
		return api, errors.New("cannot get coinbaseproAPI from server config is not of type CoinbaseproAPI")
	}

	v, err := json.Marshal(s.API)
	if err != nil {
		return api, errors.Wrap(err, "cannot get coinbaseproAPI bad bytes")
	}

	err = json.Unmarshal(v, &api)
	if err != nil {
		return api, errors.Wrap(err, "cannot get coinbaseproAPI from server config")
	}

	return api, nil
}
