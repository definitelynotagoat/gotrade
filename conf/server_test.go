package conf

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

func Test_NewServer(t *testing.T) {
	logger := zap.NewNop()

	type args struct {
		conf string
	}

	cases := []struct {
		name    string
		args    args
		wantErr bool
		server  *Server
	}{
		{
			name: "successful coinbasepro",
			args: args{
				conf: "./.mockdata/test_coinbasepro.json",
			},
			wantErr: false,
			server: &Server{
				Name: "coinbasepro",
				Symbols: []string{
					"BTC-USD",
				},
				Capital: 10000,
				logger:  logger,
				conf:    "./.mockdata/test_coinbasepro.json",
			},
		},
		{
			name: "successful iex",
			args: args{
				conf: "./.mockdata/test_iex.json",
			},
			wantErr: false,
			server: &Server{
				Name: "iex",
				API: map[string]interface{}{
					"token": "ffadfdasa",
				},
				Symbols: []string{
					"BTC-USD",
				},
				Capital: 10000,
				logger:  logger,
				conf:    "./.mockdata/test_iex.json",
			},
		},
		{
			name: "malformed conf",
			args: args{
				conf: "./.mockdata/test_badconf.json",
			},
			wantErr: true,
			server:  nil,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			server, err := NewServer(tc.args.conf, logger)
			if tc.wantErr {
				assert.NotNil(t, err)
				assert.Nil(t, server)
			} else {
				assert.Nil(t, err)
				assert.Equal(t, tc.server, server)
			}
		})
	}
}

func Test_GetIEXAPI(t *testing.T) {
	logger := zap.NewNop()

	type args struct {
		conf string
	}

	cases := []struct {
		name    string
		args    args
		wantErr bool
		iexAPI  IEXAPI
	}{
		{
			name: "successful",
			args: args{
				conf: "./.mockdata/test_iex.json",
			},
			wantErr: false,
			iexAPI: IEXAPI{
				Token: "ffadfdasa",
			},
		},
		{
			name: "unsuccessful",
			args: args{
				conf: "./.mockdata/test_coinbasepro.json",
			},
			wantErr: true,
			iexAPI:  IEXAPI{},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			server, err := NewServer(tc.args.conf, logger)
			assert.Nil(t, err)
			api, err := server.GetIEXAPI()
			if tc.wantErr {
				assert.NotNil(t, err)
			} else {
				assert.Nil(t, err)
				assert.Equal(t, tc.iexAPI, api)
			}
		})
	}
}
